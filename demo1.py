#!/usr/bin/env python
import mimejson

import PIL.Image
img=PIL.Image.open("/usr/share/pixmaps/ubuntu-logo.png")

# the multimedia data that we want to serialise
data={'image':img}

## serialisation using local filesystem
with mimejson.MIMEJSON() as mj:
  print "original object", data
  r=mj.dumps(data)
  print "serialised object",r,type(r)
  rdata=mj.loads(r)
  print "reconstructed object", rdata
