#!/usr/bin/env python
import mimejson

if False:
    import PIL.Image
    img=PIL.Image.open("/usr/share/pixmaps/ubuntu-logo.png")

    # the multimedia data that we want to serialise
    data={'image':img}

    ## serialisation using local filesystem
    with mimejson.MIMEJSON() as mj:
      print "original object", data
      r=mj.dumps(data)
      print "serialised object",r,type(r)
      rdata=mj.loads(r)
      print "reconstructed object", rdata


import cv2
img=cv2.imread("/usr/share/pixmaps/debian-logo.png")
if img is None:
    print("Error loading image")


data={'image':img, 'a': 3}

# testing cv2 images
with mimejson.MIMEJSON() as mj:
  print "original object", data
  r=mj.dumps(data)
  print "serialised object",r,type(r)
  rdata=mj.loads(r)
  print "reconstructed object", rdata

