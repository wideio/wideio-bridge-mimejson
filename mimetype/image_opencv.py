# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# Copyright (c) WIDE IO LTD
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the WIDE IO LTD nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

import cv2
import uuid
import os.path
import numpy


class Serializer:
    mimetype = "image/cvimage"

    @staticmethod
    def can_apply(obj):
        if type(obj) is dict:
            return False

        if hasattr(obj, "__class__"):
            if obj.__class__ in [numpy.ndarray, ]:
                if not obj.ndim == 3:
                    return False
                if len(obj) > 0:
                    # second dim
                    if obj[0].__class__ in [numpy.ndarray, ]:
                        if len(obj[0]) > 0:
                            # third dim
                            if obj[0][0].__class__ in [numpy.ndarray, ]:
                                if len(obj[0][0]) == 3:
                                    # RGB
                                    if obj[0][0][0].__class__ in [numpy.uint8, ]:
                                        return True
                                elif len(obj[0][0]) == 1:
                                    # grayscale
                                    if obj[0][0][0].__class__ in [numpy.uint8, ]:
                                        return True
                        else:
                            return False  # todo: Image of size zero. Should we reject it?
                else:
                    return False  # todo: Image of size zero. Should we reject it?
        return False

    @classmethod
    def serialize(cls, obj, pathdir):
        if not os.path.exists(pathdir):
            os.mkdir(pathdir)
        # for now encode saves to the pathdir of the jsonfile
        fn = os.path.join(pathdir, "%s.npy" % (uuid.uuid1(),))
        numpy.save(fn, obj)
        return {'$path$': fn, '$length$': os.stat(fn).st_size,
                '$mimetype$': cls.mimetype}

    @staticmethod
    def deserialize(obj, pathdir):
        #p = os.path.join(pathdir, obj['$path$'])
        print("path = %r" % pathdir)
        return numpy.load(pathdir)
