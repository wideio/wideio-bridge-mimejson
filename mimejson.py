#!/usr/bin/env python2
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
# Copyright (c) WIDE IO LTD
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the WIDE IO LTD nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
"""
MIMEJSON:

mimejson serializer/deserializer,
it can be use as a simple serializer or to send files to specific urls

"""
import sys
import json
import os
import errno
import imp
import os.path
import urllib
import requests
import atexit

_CAN_APPLY = 0
_SERIALIZE = 1
_DESERIALIZE = 2


def _traverse_object(obj, fct, key=None):
    """
    brute force recursif _traverse_object function
    take obj (object) and fct (function pointer)
    apply fct to every object in obj and obj itself
    """
    if type(obj) in [list, dict, tuple]:
        if type(obj) == list:
            obj = map(lambda o2: _traverse_object(o2, fct), obj)
        elif type(obj) == dict:
            obj = dict(map(lambda o2: (o2[0], _traverse_object(o2[1], fct,
                                                               key=o2[0])),
                           obj.items()))
        elif type(obj) == tuple:
            obj = tuple(map(lambda o2: _traverse_object(o2, fct), obj))
    return fct(obj, key=key)


class Connection:
    """
    if cookies timeout the connection will probably be broken and go to
    the except where cookies will be reset
    """

    def __init__(self, server, user, password):
        """
        take user, password and server url to send
        every (mimejsontype) items from the mimejson dict
        """
        self.url = server
        self.auth = (user, password)

    def send(self, files, data, url=None):
        """
        take the sending file path and his mimejson type.

        the way i upload file can change but it's the more
        easier way to do for now
        files = {
            image1: <opend file>,
            image2: <opend file>,
            file1 : <opend file>,
            ...
        }
        data = {
            image1: "{'$MIMETYPE$': 'image/jpg', ...}",
            image2: "{'$MIMETYPE$': 'image/png', ...}",
            file1: "{'$MIMETYPE$': 'file', ...}",
            data1: "{'$MIMETYPE$': 'file', ...}",
            comment: "comment"
        }
        """
        surl = self.url
        if url:
            surl = url
        res = requests.post(surl, auth=self.auth, files=files, data=data)
        return json.loads(res.text)


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


class MIMEJSON:

    def __init__(self, server=None, user=None, password=None, basepath=None, basepathistemporary=True):
        """
        if no arguments given the deserialize will not send to the server.
        you can change those following variable after init:
            - self.pathdir ---> directory to the serialized files
        """
        self.dictionary = None             # < currently opened json
        # self.pathdir = (basepath if basepath else (os.getcwd() if not
        # basepathistemporary else
        # (os.path.join(os.getcwd(),".mjson-"+str(os.getpid())))) )         # <
        # current directory (or url)
        self.pathdir = (basepath if basepath else (os.getcwd() if not basepathistemporary else (
            os.path.join("", ".mjson-" + str(os.getpid())))))         # < current directory (or url)

        self.mime_converters = {}           # < converter dict
        self.conn = None                    # < class Connection
        self._send = False                  # < bool allow send files
        self._opend_list = {}               # < list of opend file to send
        self.basepathistemporary = basepathistemporary
        if (os.path.exists(self.pathdir) and self.basepathistemporary and (len(os.listdir(self.pathdir)) > 2)):
            raise Exception, "Temporary must be a non existent|Non-empty folder as it will be deleted after completion of the program"
        if not os.path.exists(self.pathdir):
            mkdir_p(self.pathdir)
        if server is not None and user is not None and password is not None:
            self.conn = Connection(server, user, password)
        self._init_converter("mimetype")    # < init converter dict
        if self.basepathistemporary:
            atexit.register(self.deldir_if_stil_exists)

    def deldir_if_stil_exists(self):
        import shutil
        try:
            if self.basepathistemporary:
                if os.path.exists(self.pathdir):
                    shutil.rmtree(self.pathdir)
        except:
            pass

    def __mimejson_serialize_object(self, obj, key):
        """ for each obj of the dictionary, this fct is call """
        ret = obj
        for mc in self.mime_converters.items():
            can_apply = mc[1].can_apply
            if can_apply(obj):
                ret = mc[1].serialize(obj, self.pathdir)
                if self._send and key is not None and '$path$' in ret:
                    c = self.mime_converters['file']
                    self._opend_list[key] = c.deserialize(ret, ret['$path$'])
        return ret

    def __mimejson_deserialize_object(self, obj, key):
        """ for each obj of the dictionary, this fct is call """
        if type(obj) == dict and "$mimetype$" in obj:
            if (obj["$mimetype$"]) in self.mime_converters:
                path = obj['$path$']
                f = None
                if (path.startswith("http")):
                    f = urllib.urlretrieve(obj['$path$'])
                    path = f[0]
                obj = self.mime_converters[obj["$mimetype$"]].deserialize(obj,
                                                                          path)
                if f is not None:
                    os.unlink(f[0])
            else:
                sys.stderr.write(
                    "mimejson warning: unsupported mime object: %s\n" % (obj['$mimetype$'],))
        return obj

    def _mimejson_serialize_object(self, obj):
        # print "## so"
        return _traverse_object(obj, self.__mimejson_serialize_object)

    def _mimejson_deserialize_object(self, obj):
        return _traverse_object(obj, self.__mimejson_deserialize_object)

    def _load_class_from_file(self, path, expected_class):
        """
        return and load expected_class from path file
        """
        mod_name, file_ext = os.path.splitext(os.path.split(path)[-1])
        if file_ext.lower() == '.py':
            py_mod = imp.load_source(mod_name, path)
        elif file_ext.lower() == '.pyc':
            py_mod = imp.load_compiled(mod_name, path)
        if hasattr(py_mod, expected_class):
            return getattr(py_mod, expected_class)()
        return None

    def _init_converter(self, folder_converters):
        """
        load every converter mimetype from mimetype folder
        converters must be found in mimetype/converters.py
        with an Serializer class
        """
        edir = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            folder_converters)
        for f in os.listdir(edir):
            if f.endswith("py") and not f.startswith("__init__"):
                try:
                    e = self._load_class_from_file(
                        os.path.join(edir, f), 'Serializer')
                    if e is not None:
                        self.register_converter(e)
                except:
                    sys.stderr.write(
                        'mimejson warning: unable to load module %r\n' % (f,))

    def _deserialize_all(self):
        """
        deserialize jsondict $path$ (path/url) into object (from mimetype)
        need a mimejson dict loaded to work
        """
        self.dictionary = self._mimejson_deserialize_object(self.dictionary)

    def _serialize_all(self):
        """
        serialize in self.pathdir and send toward server if loaded.
        need a mimejson dict loaded to work
        change self.pathdir to change the path of serialized files
        """
        self.dictionary = self._mimejson_serialize_object(self.dictionary)

    def register_converter(self, class_converter):
        """
        add converter function
        mimetype can be string or tuple of string
        """
        if type(class_converter.mimetype) is tuple:
            for m in class_converter.mimetype:
                self.mime_converters[m] = class_converter
                # change this by classe (fct_can_apply, fct_enc, fct_dec)
        else:
            self.mime_converters[class_converter.mimetype] = class_converter
            # (fct_can_apply, fct_enc, fct_dec)

    def dumps(self, data=None, send=True, url=None):
        """
        dumps the mimejson from dict to self.pathdir/dump.mimejson
        then send it to the server

        return a dict if it has be sent
        otherwise the path
        """
        if data != None:
            self.dictionary = data
        allow = (int, str, unicode, bool, float)
        self._send = send
        self._serialize_all()
        # else:
        #  sys.stderr.write("mimejson warning: key dropped %s : %r\n"%(k,type(self.dictionary[k])))
        if send is True and self.conn is not None:
            data = {}
            for k in self.dictionary:
                if type(self.dictionary[k]) not in allow:
                    data[k] = json.dumps(self.dictionary[k])
                else:
                    data[k] = self.dictionary[k]
            ret = self.conn.send(data=data, files=self._opend_list, url=url)
            for k in self._opend_list:
                self._opend_list[k].close()
            self._opend_list = {}
            return json.dumps(self.dictionary), ret
        return json.dumps(self.dictionary)

    def dump(self, data, url=None):
        return self.dumps(data, False, url=url)

    def load(self, fp):  # aka path
        """
        load url/path/file.mimejson
        """
        if os.path.isfile(fp):
            jsonfile = open(fp, 'r+')
        else:
            x = urllib.urlretrieve(fp)
            jsonfile = open(x[0], "r")
            os.unlink(x[0])
        # make dict from json
        self.dictionary = json.load(jsonfile)
        jsonfile.close()
        self._deserialize_all()

    def loads(self, string):
        """
        load string json
        """
        self.dictionary = json.loads(string)
        self._deserialize_all()
        return self.dictionary

    def loadd(self, d):
        """
        load dict
        """
        self.dictionary = d
        self._deserialize_all()
        return self.dictionary

    def loadc(self, obj):
        """
        load 'Connection' instance
        """
        self.conn = obj

    def __enter__(self):
        return self

    def __exit__(self, *args):
        if self.basepathistemporary:
            for f in os.listdir(self.pathdir):
                os.unlink(os.path.join(self.pathdir, f))
            os.rmdir(self.pathdir)


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        # mimejson = MIMEJSON('localhost:8005', 'admin', 'admin')
        mimejson = MIMEJSON('wio-000.home:8005', 'admin', 'admin')
        print "-----------deserialize:"
        mimejson.load(sys.argv[1])
        print mimejson.dictionary
        print
        print "-----------serialize:"
        mimejson.dumps()
        print mimejson.dictionary
    else:
        print "Usage:\n./mimejson.py mimejson/file/path.json"
